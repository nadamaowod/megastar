package com.example.megastar.model

import androidx.room.TypeConverter
import com.example.megastar.model.entities.Data
import com.example.megastar.model.entities.Item

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class Converters {
    var gson = Gson()

    @TypeConverter
    fun fromSource(data: Data): String {
        return gson.toJson(data)
    }

    @TypeConverter
    fun toSource(gason: String):Data {
        val data = object : TypeToken<Data>() {
        }.type
        return gson.fromJson(gason, data)
    }
    @TypeConverter
    fun fromItem(item: Item): String {
        return gson.toJson(item)
    }

    @TypeConverter
    fun toItem(gason: String):Item {
        val item = object : TypeToken<Item>() {
        }.type
        return gson.fromJson(gason, item)
    }


}
