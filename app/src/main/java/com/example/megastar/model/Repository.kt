package com.example.megastar.model

import android.content.Context
import com.example.megastar.model.db.GeneralDataBase
import com.example.megastar.model.entities.Item
import com.example.megastar.model.entities.countryModel
import com.example.megastar.model.network.ApiBase
import com.example.megastar.model.network.ApiClient
import com.example.megastar.model.network.ApiInterface


import retrofit2.Response


class Repository (var context: Context){

    private var apiClient: ApiClient? = null
    private var apiInterface: ApiInterface? = null

    private lateinit var generalDb: GeneralDataBase

    init {
        apiClient = ApiClient()
        apiInterface =
            apiClient!!.getClient(ApiBase.BASE_URL)!!.create(ApiInterface::class.java)
        generalDb= GeneralDataBase.getDataClient(context)
    }



   suspend fun getCountries(): Response<countryModel> {
        return apiInterface!!.getCountries()
    }

   // suspend fun insert(item: Item) = generalDb.CountriesDaoInstance().insert(item)




}
