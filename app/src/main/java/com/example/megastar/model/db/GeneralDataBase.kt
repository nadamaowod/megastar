package com.example.megastar.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.megastar.model.Converters
import com.example.megastar.model.dao.CountriesDao
import com.example.megastar.model.entities.Item

@Database(entities = [Item::class], version =4 )
abstract class GeneralDataBase : RoomDatabase() {

    abstract fun CountriesDaoInstance() : CountriesDao


    companion object {

        @Volatile
        private var INSTANCE: GeneralDataBase? = null

        fun getDataClient(context: Context) : GeneralDataBase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, GeneralDataBase::class.java, "MEGA_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }


}