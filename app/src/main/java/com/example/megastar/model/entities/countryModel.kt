package com.example.megastar.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


data class countryModel(
    val baseUrl: String,
    val code: Int,
    val `data`: Data,
    val errors: String,
    val message: String,
    val success: Boolean

)


data class Data(
    val currentPage: Int,
    val items: List<Item>,
    val pageSize: Int,
    val totalItems: Int,
    val totalPages: Int,
    val totalUnSeen: Int
)
@Entity(tableName = "Country_Table")
data class Item(
    val arName: String,
    val countryCallingCode: String,
    val countryCode: String,
    val countryFlag: String,
    val creationDate: String,
    val deleterUserId: String,
    val deletionDate: String,
    val enName: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val isDeleted: String,
    val lastModificationDate: String,
    val lastModifierUserId: String,
    val spaces: String,
    val fav:Int =0
//o for unFav
//1 for fav

)
