package com.example.megastar.model.network

enum class ApiBase(baseurl: String) {
    BASE_URL("http://5.9.120.72:4777/preProduction/api/Country/");

    var baseUrl = "http://5.9.120.72:4777/preProduction/api/Country/"

    override fun toString(): String {
        return baseUrl
    }

    init {
        baseUrl = baseurl
    }
}
