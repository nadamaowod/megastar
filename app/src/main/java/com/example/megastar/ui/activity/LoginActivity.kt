package com.example.megastar.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.megastar.R
import com.example.megastar.databinding.ActivityLoginBinding
import com.example.megastar.ui.activity.Main.MainActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity() {


    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.btnHome.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)


        })
    }
}