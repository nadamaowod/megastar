package com.example.megastar.ui.activity.Main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.megastar.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}