package com.example.megastar.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.megastar.R
import com.example.megastar.model.entities.Item
import kotlinx.android.synthetic.main.item_countrtes.view.*


class countriesAdapter : RecyclerView.Adapter<countriesAdapter.countrieSViewHolder>() {

    inner class countrieSViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differCallback = object : DiffUtil.ItemCallback<Item>() {
        override
        fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.id == newItem.id
        }

        override
        fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): countrieSViewHolder {
        var binding = (LayoutInflater.from(parent.context).inflate(
            R.layout.item_countrtes,
            parent,
            false
        ))
        return countrieSViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    private var onItemClickListener: ((Item) -> Unit)? = null

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: countrieSViewHolder, position: Int) {
        val country = differ.currentList[position]
        holder.itemView.apply {
            val photo = "http://5.9.120.72:4777/preProduction/" + country.countryFlag

            Glide.with(this).load(photo).into(img_flag)
            tv_name.text = country.enName
            tv_tel.text = country.countryCallingCode


            img_fav.setOnClickListener {
                onItemClickListener?.let {
                    it(country)
                }
            }
            card.setOnClickListener {
                onItemClickListener?.let {
                    it(country)
                }
            }
        }
    }

    fun setOnItemClickListener(listener: (Item) -> Unit) {
        onItemClickListener = listener
    }

}







