package com.example.megastar.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.R
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.example.megastar.databinding.FragmentDataBinding
import com.example.megastar.model.entities.Item
import kotlinx.android.synthetic.main.fragment_data.*
import kotlinx.android.synthetic.main.item_countrtes.view.*
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.navigation.Navigation
import com.example.megastar.model.Converters
import com.example.megastar.util.Util
import com.google.gson.Gson
import com.google.gson.GsonBuilder


class DataFragment : Fragment() {

    private lateinit var binding: FragmentDataBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            com.example.megastar.R.layout.fragment_data,
            container,
            false
        )

        setupData()
        onClick()

        return binding.root
    }

    private fun onClick() {
        binding.imgFav.setOnClickListener(View.OnClickListener {
           /*   val res = resources.getDrawable(R.drawable.fav)
              img_fav.setImageDrawable(res)*/
        })
        binding.imgBack.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(com.example.megastar.R.id.action_dataFragment_to_homeFragment)
        })
    }

    private fun setupData() {
        val data = requireArguments().get("KEY").toString()
        val convert: Converters = Converters()
        val countryData: Item = convert.toItem(data)

        val photo = "http://5.9.120.72:4777/preProduction/" + countryData.countryFlag
        Glide.with(this@DataFragment).load(photo).into(binding.imgFlag)
        binding.tvName.setText(countryData.enName)
        binding.tvTel.setText(countryData.countryCallingCode)

    }

}