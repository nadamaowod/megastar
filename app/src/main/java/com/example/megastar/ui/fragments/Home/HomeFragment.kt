package com.example.megastar.ui.fragments.Home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.megastar.R
import com.example.megastar.databinding.FragmentHomeBinding
import com.example.megastar.model.Converters
import com.example.megastar.ui.adapter.countriesAdapter

class HomeFragment : Fragment() {

    lateinit var homeVm: HomeViewModel
    lateinit var binding: FragmentHomeBinding

    lateinit var countriesAdapter: countriesAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        //init VM

        homeVm = activity?.let { ViewModelProvider(it).get(HomeViewModel::class.java) }!!
        activity?.let { homeVm.setActivity(it) }
        showProgressBar()

        setupRecyclerView()

        onclick()


        homeVm.countries.observe(viewLifecycleOwner, Observer { response ->

            hideProgressBar()
            response.data?.let { newsResponse ->
                countriesAdapter.differ.submitList(newsResponse.items)
            }
        })
        binding.lifecycleOwner = this@HomeFragment
        return binding.root

    }

    private fun onclick() {
        binding.imgBack.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_homeFragment_to_loginActivity)

        })
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun setupRecyclerView() {
        countriesAdapter = countriesAdapter()
        binding.rvcountries.apply {
            adapter = countriesAdapter
            layoutManager = LinearLayoutManager(activity)
            countriesAdapter.setOnItemClickListener {

                val args = Bundle()
                val convert: Converters = Converters()
                var JsonString = convert.fromItem(it)
                args.putString("KEY", JsonString)

                Navigation.findNavController(binding.root)
                    .navigate(R.id.action_homeFragment_to_dataFragment, args)
            }
        }
    }

}