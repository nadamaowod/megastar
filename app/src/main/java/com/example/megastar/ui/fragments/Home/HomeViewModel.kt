package com.example.megastar.ui.fragments.Home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.megastar.model.Repository
import com.example.megastar.model.entities.countryModel

import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Response

class HomeViewModel : ViewModel() {
    private lateinit var repository: Repository
    private lateinit var context: Context
    val countries: MutableLiveData<countryModel> = MutableLiveData()
    val searchcountries: MutableLiveData<List<countryModel>> = MutableLiveData()

    fun setActivity(context: Context) {
        this.context = context
        repository = Repository(context)
        getCountries()
    }


    fun getCountries() {
        viewModelScope.launch {
            //  breakingNews.postValue(Resource.Loading())
            val response = repository.getCountries()
            if (response.isSuccessful) {
                countries.postValue(response.body())
            }
        }
    }
}


