package com.example.megastar.util

import com.google.gson.GsonBuilder

import com.google.gson.Gson


class Util {
    private var gson: Gson? = null

    public  fun getGsonParser(): Gson? {
        if (null == gson) {
            val builder = GsonBuilder()
            gson = builder.create()
        }
        return gson
    }
}